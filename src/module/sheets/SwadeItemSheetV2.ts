import { ItemDataSource } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData';
import { AdditionalStats, EquipState, ItemActions } from '../../globals';
import { ItemAction } from '../../interfaces/additional.interface';
import ActiveEffectWizard from '../apps/ActiveEffectWizard';
import SwadeDocumentTweaks from '../apps/SwadeDocumentTweaks';
import { SWADE } from '../config';
import { constants } from '../constants';
import SwadeActiveEffect from '../documents/active-effect/SwadeActiveEffect';
import SwadeActor from '../documents/actor/SwadeActor';
import SwadeItem from '../documents/item/SwadeItem';
import { ItemGrant } from '../documents/item/SwadeItem.interface';
import { Logger } from '../Logger';
import { Accordion } from '../style/Accordion';
import { copyToClipboard } from '../util';

export default class SwadeItemSheetV2 extends ItemSheet {
  collapsibleStates: CollapsibleStates = {
    powers: {},
    actions: {},
    effects: {},
  };
  _effectCreateDropDown: ContextMenu;

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 600,
      height: 560,
      classes: ['swade-item-sheet', 'swade', 'swade-app'],
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'summary',
        },
      ],
      scrollY: ['.properties', '.actions', '.editor-container .editor-content'],
      dragDrop: [{ dropSelector: null, dragSelector: '.effect-list li' }],
      resizable: true,
    });
  }

  override get template(): string {
    return `systems/swade/templates/item/${this.type}.hbs`;
  }

  get type(): this['item']['data']['type'] {
    return this.item.type;
  }

  get hasInlineDelete(): boolean {
    const types = ['edge', 'hindrance', 'ability', 'skill', 'power', 'action'];
    return types.includes(this.type);
  }

  get isPhysicalItem(): boolean {
    const types = [
      'weapon',
      'armor',
      'shield',
      'gear',
      'consumable',
      'container',
    ];
    return types.includes(this.type);
  }

  get actionTypes(): Record<string, string> {
    return {
      trait: 'SWADE.Trait',
      damage: 'SWADE.Dmg',
      resist: 'SWADE.Resist',
      macro: 'DOCUMENT.Macro',
    };
  }

  get macroActorTypes(): Record<string, string> {
    return {
      default: 'SWADE.MacroActor.Default',
      self: 'SWADE.MacroActor.Self',
    };
  }

  override activateListeners(html: JQuery<HTMLElement>): void {
    super.activateListeners(html);
    this._setupAccordions();
    this._setupEffectCreateMenu(html);

    html.find('.profile-img').on('contextmenu', () => {
      if (!this.item.img) return;
      new ImagePopout(this.item.img, {
        title: this.item.name!,
        shareable: this.item?.isOwner ?? game.user?.isGM,
      }).render(true);
    });

    if (!this.isEditable) return;

    this.form?.addEventListener('keypress', (ev: KeyboardEvent) => {
      const target = ev.target as HTMLButtonElement;
      const targetIsButton = 'button' === target?.type;
      if (!targetIsButton && ev.key === 'Enter') {
        ev.preventDefault();
        this.submit({ preventClose: true });
        return false;
      }
    });

    // Delete Item from within Sheet. Only really used for Skills, Edges, Hindrances and Powers
    html.find('.inline-delete').on('click', () => this.item.delete());

    html.find('.add-action').on('click', () => {
      const id = foundry.utils.randomID(8);
      this.collapsibleStates[id] = true;
      this.item.update({
        ['system.actions.additional.' + id]: {
          name: game.i18n.format('DOCUMENT.New', {
            type: game.i18n.localize('TYPES.Item.action'),
          }),
          type: constants.ACTION_TYPE.TRAIT,
        },
      });
    });

    html.find('.action-delete').on('click', async (ev) => {
      const id = ev.currentTarget.dataset.actionId;
      const action = getProperty(
        this.item,
        `system.actions.additional.${id}`,
      ) as ItemAction;
      const text = game.i18n.format('SWADE.DeleteEmbeddedActionPrompt', {
        action: action.name,
      });
      await Dialog.confirm({
        content: `<p class="text-center">${text}</p>`,
        yes: () => {
          this.item.update({
            'system.actions.additional': {
              [`-=${id}`]: null,
            },
          });
        },
        defaultYes: false,
        options: foundry.utils.mergeObject(Dialog.defaultOptions, {
          classes: ['dialog', 'swade-app'],
        }),
      });
    });

    html.find('.power-delete').on('click', async (ev) => {
      const id = $(ev.currentTarget).parents('details').data('powerId');
      const power = this.item.embeddedPowers.get(id);
      const text = game.i18n.format('SWADE.DeleteEmbeddedPowerPrompt', {
        power: power?.name,
      });
      await Dialog.confirm({
        content: `<p class="text-center">${text}</p>`,
        yes: async () => await this._deleteEmbeddedDocument('power', id),
        defaultYes: false,
        options: foundry.utils.mergeObject(Dialog.defaultOptions, {
          classes: ['dialog', 'swade-app'],
        }),
      });
    });

    html.find('.grant-delete').on('click', async (ev) => {
      const uuid = $(ev.currentTarget).parents('.granted-item').data('uuid');
      const grants = this.item.grantsItems;
      grants.findSplice((v) => v.uuid === uuid);
      await this.item.update({ 'system.grants': grants });
    });

    html.find('.grant-name').on('click', async (ev) => {
      const uuid = $(ev.currentTarget).parents('.granted-item').data('uuid');
      const doc = (await fromUuid(uuid)) as SwadeItem;
      doc?.sheet?.render(true);
    });

    html.find('.effect-action').on('click', (ev) => {
      ev.preventDefault();
      ev.stopPropagation();
      const a = ev.currentTarget;
      const effectId = a.closest('details')!.dataset.effectId! as string;
      const effect = this.item.effects.get(effectId, { strict: true });
      const action = a.dataset.action as string;
      const toggle = a.dataset.toggle as string;

      switch (action) {
        case 'edit':
          return effect.sheet?.render(true);
        case 'delete':
          return effect.delete();
        case 'toggle':
          return effect.update(this._toggleEffect(effect, toggle));
      }
    });

    html.find('.delete-embedded').on('click', async (ev) => {
      const id = ev.currentTarget.dataset.id!;
      await this._deleteEmbeddedDocument('ability', id);
    });

    html.find('.power .damage').on('click', (ev) => {
      const id = $(ev.currentTarget).parents('details').data('powerId');
      const tempPower = new SwadeItem(this.item.embeddedPowers.get(id));
      tempPower.rollDamage();
    });

    html.find('.additional-stats .rollable').on('click', async (ev) => {
      const stat = ev.currentTarget.dataset.stat!;
      const statData = this.item.system.additionalStats[stat]!;
      let modifier = statData.modifier ?? '';
      if (!modifier.match(/^[+-]/)) {
        modifier = '+' + modifier;
      }
      //return of there's no value to roll
      if (!statData.value) return;
      const roll = new Roll(`${statData.value}${modifier}`);
      await roll.evaluate({ async: true });
      await roll.toMessage({
        speaker: CONFIG.ChatMessage.documentClass.getSpeaker(),
        flavor: `${this.item.name} - ${statData.label}`,
      });
    });

    html
      .find('.use-consumable')
      .on('click', async () => await this.item.consume());

    html.find('.loaded-ammo-name').on('mouseenter', async (ev) => {
      const loadedAmmo = this.item.getFlag('swade', 'loadedAmmo');
      const content = `<h3>${loadedAmmo?.name}</h3>${loadedAmmo?.system.description}`;
      game.tooltip.activate(ev.currentTarget, {
        text: await TextEditor.enrichHTML(content, { async: true }),
      });
    });
  }

  override async getData(
    options?: Partial<DocumentSheetOptions>,
  ): Promise<SwadeItemSheetData> {
    const additionalStats = this._getAdditionalStats();

    const data: SwadeItemSheetData = {
      itemType: this._getItemType(),
      enrichedDescription: await this._enrichText(this.item.system.description),
      hasInlineDelete: this.hasInlineDelete,
      isPhysicalItem: this.isPhysicalItem,
      hasCategory: this.item.canHaveCategory,
      actionTypes: this.actionTypes,
      macroActorTypes: this.macroActorTypes,
      hasAdditionalStats: Object.keys(additionalStats).length > 0,
      additionalStats: additionalStats,
      collapsibleStates: this.collapsibleStates,
      isArcaneDevice: this.item.isArcaneDevice,
      ranges: this._rangeSuggestions(),
      equipStatusOptions: this._equipStatusOptions(),
      settingRules: {
        modSlots: game.settings.get('swade', 'vehicleMods'),
        noPowerPoints: game.settings.get('swade', 'noPowerPoints'),
      },
    };

    if (this.item.type === 'ability') {
      const subtype = this.item.system.subtype;
      data.abilityConfig = {
        localization: SWADE.abilitySheet,
        abilityHeader: SWADE.abilitySheet[subtype].abilities,
        isAncestryOrArchetype:
          subtype === constants.ABILITY_TYPE.ANCESTRY ||
          subtype === constants.ABILITY_TYPE.ARCHETYPE,
      };
      data.embeddedAbilities = this._prepareEmbeddedAbilities();
    }

    if (this.item.canGrantItems) {
      data.grantedItems = await this._getGrantedItems();
    }

    for (const effect of this.item.effects) {
      foundry.utils.setProperty(
        effect,
        'enrichedDescription',
        await TextEditor.enrichHTML(effect.description, {
          async: true,
          secrets: this.item.isOwner,
        }),
      );
    }

    if (this.type === 'weapon') {
      data.ppReload = false;
      data.trademarkWeaponOptions = this._trademarkWeaponOptions();
      switch (this.item.system.reloadType) {
        case constants.RELOAD_TYPE.NONE:
        case constants.RELOAD_TYPE.SINGLE:
        case constants.RELOAD_TYPE.FULL:
          data.ammoList = this.actor?.itemTypes.gear
            .filter((i) => i.system.isAmmo)
            .map((i) => i.name) as string[];
          break;
        case constants.RELOAD_TYPE.MAGAZINE:
          data.ammoList = this.actor?.itemTypes.consumable
            .filter(
              (i) =>
                i.type === 'consumable' &&
                i.system.subtype === constants.CONSUMABLE_TYPE.MAGAZINE,
            )
            .map((i) => i.name) as string[];
          data.ammoLoaded = this.item.getFlag('swade', 'loadedAmmo')?.name;
          break;
        case constants.RELOAD_TYPE.PP:
          data.ammoList = Object.keys(this.actor?.system?.powerPoints ?? {});
          data.ppReload = true;
          break;
        case constants.RELOAD_TYPE.BATTERY:
          data.ammoList = this.actor?.itemTypes.consumable
            .filter(
              (i) =>
                i.type === 'consumable' &&
                i.system.subtype === constants.CONSUMABLE_TYPE.BATTERY,
            )
            .map((i) => i.name) as string[];
          data.ammoLoaded = this.item.getFlag('swade', 'loadedAmmo')?.name;
          break;
        case constants.RELOAD_TYPE.SELF:
          // Doesn't use external ammo
          break;
      }
      data.reloadTypeOptions = this._reloadTypeOptions();
    }

    if (this.type === 'consumable') {
      data.subtypes = {
        [constants.CONSUMABLE_TYPE.REGULAR]: 'SWADE.ConsumableType.Regular',
        [constants.CONSUMABLE_TYPE.MAGAZINE]: 'SWADE.ReloadType.Magazine',
        [constants.CONSUMABLE_TYPE.BATTERY]: 'SWADE.ReloadType.Battery',
      };
    }

    if (this.item.isArcaneDevice) {
      data.embeddedPowers = this.item.embeddedPowers;
    }
    return foundry.utils.mergeObject(await super.getData(options), data);
  }

  protected override _getHeaderButtons() {
    const buttons = super._getHeaderButtons();

    if (this.isEditable) {
      buttons.unshift({
        label: 'SWADE.DocumentTweaks',
        class: 'configure-actor',
        icon: 'fa-solid fa-gears',
        onclick: () => new SwadeDocumentTweaks(this.item).render(true),
      });
    }

    buttons.unshift({
      label: 'SWADE.DocumentLink',
      class: 'copy-link',
      icon: 'fas fa-link',
      onclick: () => copyToClipboard(this.item.link),
    });
    return buttons;
  }

  protected override _getSubmitData(updateData: object | null = {}) {
    const data = super._getSubmitData(updateData);
    if (this.item.type !== 'skill') {
      // Prevent submitting overridden values
      const overrides = foundry.utils.flattenObject(this.item.overrides);
      Object.keys(overrides).forEach((v) => delete data[v]);
    }
    return data;
  }

  protected override async _onDrop(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    try {
      //get the data
      const data = JSON.parse(event.dataTransfer!.getData('text/plain')) as {
        type: string;
        uuid: string;
      };
      switch (data.type) {
        case 'ActiveEffect':
          await this._onDropActiveEffect(event, data);
          break;
        case 'Item':
          await this._onDropItem(event, data);
          break;
        case 'Macro':
          await this._onDropMacro(event, data);
          break;
        default:
          break;
      }
    } catch (error) {
      Logger.error(error);
    }
  }

  private async _onDropActiveEffect(_event: DragEvent, data) {
    if (!this.item.isOwner || !data.data) return;
    if (await this._isSourceSameAsDestination(data)) return;
    return CONFIG.ActiveEffect.documentClass.create(data.data, {
      parent: this.item,
    });
  }

  private async _onDropItem(event: DragEvent, data) {
    const uuid = data.uuid;
    Logger.debug(
      `Trying to add ${data.type} ${uuid} to ${this.item.type}/${this.item.name}`,
    );
    const item = (await fromUuid(uuid)) as SwadeItem;

    if (item.type === 'ability' && item.system.subtype !== 'special') {
      return Logger.warn('SWADE.CannotAddAncestryToAncestry', {
        localize: true,
        toast: true,
      });
    }

    const target = event.target as HTMLElement;
    const classList = target.closest<HTMLElement>('.tab.active')?.classList;

    if (classList?.contains('properties')) {
      await this._addGrantedItem(item);
    } else if (classList?.contains('embedded')) {
      await this._addEmbedded(item);
    } else if (classList?.contains('powers')) {
      await this._addArcaneDevicePower(item);
    } else if (classList?.contains('actions')) {
      await this._addOrReplaceActions(item);
    }
  }

  private async _onDropMacro(event: DragEvent, data) {
    const target = event.target as HTMLElement;
    const actionId = target.closest<HTMLElement>('.tab.actions.active .action')
      ?.dataset.actionId as string;
    const action = this.item.system.actions.additional[actionId] as ItemAction;
    if (action.type !== constants.ACTION_TYPE.MACRO) return;
    await this.item.update({
      [`system.actions.additional.${actionId}.uuid`]: data.uuid,
    });
  }

  private async _addGrantedItem(item: SwadeItem) {
    if (
      !this.item.canGrantItems ||
      this.item.isEmbedded ||
      item.uuid === this.item.uuid
    )
      return;

    const grants = this.item.grantsItems;
    grants.push({
      name: item.name,
      img: item.img,
      uuid: item.uuid,
    });
    await this.item.update({ 'system.grants': grants });
  }

  private async _addEmbedded(_item: SwadeItem) {
    const msg =
      'Embedded Abilities have been deprecated in favor of Item Grants';
    ui.notifications.warn(msg, { permanent: true, console: false });
    foundry.utils.logCompatibilityWarning(msg, {
      since: '3.1',
      until: '4.0',
      details:
        'You can no longer add Embedded Abilities to items but they will still be able to be transferred to actors until the depreciation period ends.',
    });
  }

  private async _addArcaneDevicePower(item: SwadeItem) {
    if (!this.item.isArcaneDevice || item.type !== 'power') return;
    const collection = this.item.embeddedPowers;
    collection.set(foundry.utils.randomID(), item.toObject());
    await this._saveEmbeddedPowers(collection);
  }

  private async _addOrReplaceActions(item: SwadeItem) {
    const actionKey = 'system.actions.additional';
    const actions = foundry.utils.getProperty(this.item, actionKey) as
      | ItemActions
      | undefined;
    if (typeof actions === 'undefined') return; //no actions on this item, return before we break something;
    if (foundry.utils.isEmpty(actions)) {
      //if no actions are present then we simply copy the actions from the dropped item
      return this.item.update({
        [actionKey]: foundry.utils.getProperty(item, actionKey),
      });
    }
    //otherwise we ask to copy or replace the current actions
    const existingActions = foundry.utils.getProperty(
      item,
      actionKey,
    ) as ItemActions;
    const data: Dialog.Data = {
      title: game.i18n.localize('SWADE.AddOrReplaceActions.Title'),
      content: game.i18n.format('SWADE.AddOrReplaceActions.Content', {
        source: item.name,
        type: game.i18n.localize('TYPES.Item.' + item.type),
      }),
      default: 'add',
      buttons: {
        add: {
          label: game.i18n.localize('SWADE.AddOrReplaceActions.Add'),
          icon: '<i class="fa-solid fa-copy"></i>',
          callback: () => {
            const newActions: ItemActions = {};
            //give the actions new keys to make sure there are no id collisions
            for (const action of Object.values(existingActions)) {
              newActions[randomID(8)] = action;
            }
            this.item.update({ [actionKey]: newActions });
          },
        },
        replace: {
          label: game.i18n.localize('SWADE.AddOrReplaceActions.Replace'),
          icon: '<i class="fa-solid fa-rotate"></i>',
          callback: () =>
            this.item.update(
              { [actionKey]: existingActions },
              { recursive: false, diff: false },
            ),
        },
      },
    };
    new Dialog(data, { classes: ['dialog', 'swade-app'] }).render(true);
  }

  /** Is the drop data coming from the same item? */
  private async _isSourceSameAsDestination(data) {
    let other;

    //item owned by token actor
    if (data.sceneId && data.tokenId) {
      other = game.scenes
        ?.get(data.sceneId)
        ?.tokens.get(data.tokenId)
        ?.actor?.items.get(data.itemId);
    }

    //standalone item
    if (!other && data.itemId) {
      if (data.pack) {
        other = await game.packs.get(data.pack)?.getDocument(data.itemId);
      } else {
        other = game.items?.get(data.itemId);
      }
    }

    //item owned by standalone actor
    if (!other && data.actorId) {
      if (data.pack) {
        const actor = (await game.packs
          .get(data.pack)
          ?.getDocument(data.actorId)) as StoredDocument<SwadeActor>;
        other = actor?.items.get(data.itemId);
      } else {
        other = game.actors?.get(data.actorId)?.items.get(data.itemId);
      }
    }
    return this.item === other;
  }

  protected override async _onDragStart(event: DragEvent) {
    const src = event.target as HTMLElement;

    // Create drag data
    const dragData: Record<string, unknown> = {
      actorId: this.actor?.id,
      sceneId: this.actor?.isToken ? canvas.scene?.id : null,
      tokenId: this.actor?.isToken ? this.actor?.token?.id : null,
      itemId: this.item.id,
      pack: this.item.pack,
    };

    // Active Effect
    if (src.dataset.effectId) {
      const effect = this.item.effects.get(src.dataset.effectId);
      dragData.type = 'ActiveEffect';
      dragData.data = effect?.toObject();
    }

    // Set data transfer
    event.dataTransfer?.setData('text/plain', JSON.stringify(dragData));
  }

  private async _deleteEmbeddedDocument(type: 'power' | 'ability', id: string) {
    const flagKey = {
      ability: 'embeddedAbilities',
      power: 'embeddedPowers',
    };
    const flagContent = this.item.getFlag('swade', flagKey[type]) ?? [];
    const map = new Map(flagContent as Array<[string, ItemDataSource]>);
    map.delete(id);
    this.item.setFlag('swade', flagKey[type], Array.from(map));
  }

  /** @deprecated */
  private _prepareEmbeddedAbilities(): Array<Record<string, unknown>> {
    const collection = this.item.embeddedAbilities;
    const items = new Array<Record<string, unknown>>();
    for (const [key, val] of collection) {
      const type =
        val.type === 'ability'
          ? game.i18n.localize('SWADE.SpecialAbility')
          : game.i18n.localize(`TYPES.Item.${val.type}`);

      let majorMinor = '';
      if (val.type === 'hindrance') {
        if (val.data?.major ?? val.system.major) {
          majorMinor = game.i18n.localize('SWADE.Major');
        } else {
          majorMinor = game.i18n.localize('SWADE.Minor');
        }
      }
      items.push({
        id: key,
        img: val.img,
        name: val.name,
        type,
        majorMinor,
      });
    }
    return items;
  }

  private async _saveEmbeddedPowers(map: Map<string, ItemDataSource>) {
    return this.item.setFlag('swade', 'embeddedPowers', Array.from(map));
  }

  private _getAdditionalStats(): AdditionalStats {
    const stats = foundry.utils.deepClone(
      this.item.system.additionalStats,
    ) as AdditionalStats;
    for (const [key, attr] of Object.entries(stats)) {
      if (attr.dtype === 'Selection') {
        const options = game.settings.get('swade', 'settingFields').item;
        const optionString = options[key].optionString ?? '';
        attr.options = optionString
          .split(';')
          .reduce((a, v) => ({ ...a, [v.trim()]: v.trim() }), {});
      }
    }
    return stats;
  }

  private _getGrantedItems(): ItemGrant[] {
    if (!this.item.canGrantItems) return [];
    const grants = this.item.grantsItems;
    const enriched = new Array<ItemGrant>();
    for (const grant of grants) {
      const item = fromUuidSync(grant.uuid) as SwadeItem | null;
      enriched.push({
        name: grant.mutation?.name ?? item?.name ?? grant.name,
        img: grant.mutation?.img ?? item?.img ?? grant.img,
        uuid: grant.uuid,
        missing: !item,
      });
    }
    return enriched;
  }

  private _getItemType(): string {
    if (this.type === 'ability') {
      const subtype = this.item.system.subtype;
      switch (subtype) {
        case constants.ABILITY_TYPE.ANCESTRY:
          return SWADE.abilitySheet.ancestry.dropdown;
        case constants.ABILITY_TYPE.ARCHETYPE:
          return SWADE.abilitySheet.archetype.dropdown;
        default:
          return SWADE.abilitySheet.special.dropdown;
      }
    }
    return `TYPES.Item.${this.type}`;
  }

  private async _enrichText(text: string): Promise<string> {
    const enriched = await TextEditor.enrichHTML(text, {
      async: true,
      secrets: this.isEditable,
    });
    return enriched;
  }

  private _setupAccordions() {
    this.form
      ?.querySelectorAll<HTMLDetailsElement>('.actions-list details')
      .forEach((el) => {
        new Accordion(el, '.content', { duration: 200 });
        const id = el.dataset.actionId as string;
        el.querySelector('summary')?.addEventListener('click', () => {
          const states = this.collapsibleStates.actions;
          const currentState = Boolean(states[id]);
          states[id] = !currentState;
        });
      });

    this.form
      ?.querySelectorAll<HTMLDetailsElement>('.powers-list details')
      .forEach((el) => {
        new Accordion(el, '.content', { duration: 200 });
        const id = el.dataset.powerId as string;
        el.querySelector('summary')?.addEventListener('click', () => {
          const states = this.collapsibleStates.powers;
          const currentState = Boolean(states[id]);
          states[id] = !currentState;
        });
      });

    this.form
      ?.querySelectorAll<HTMLDetailsElement>('.effect-list details')
      .forEach((el) => {
        new Accordion(el, '.content', { duration: 200 });
        const id = el.dataset.effectId as string;
        el.querySelector('summary')?.addEventListener('click', () => {
          const states = this.collapsibleStates.effects;
          const currentState = Boolean(states[id]);
          states[id] = !currentState;
        });
      });
  }

  private _setupEffectCreateMenu(html: JQuery<HTMLElement> = $('body')) {
    this._effectCreateDropDown = new ContextMenu(
      html,
      '.effects .header',
      [
        {
          name: 'SWADE.ActiveEffects.AddGuided',
          icon: '<i class="fa-solid fa-hat-wizard"></i>',
          condition: this.object.isOwner,
          callback: (_li) => {
            new ActiveEffectWizard(this.object).render(true);
          },
        },
        {
          name: 'SWADE.ActiveEffects.AddUnguided',
          icon: '<i class="fa-solid fa-file-plus"></i>',
          condition: this.object.isOwner,
          callback: (_li) => {
            this._createActiveEffect();
          },
        },
      ],
      { eventName: 'click' },
    );
  }

  private async _createActiveEffect() {
    const newEffect = await CONFIG.ActiveEffect.documentClass.create(
      {
        name: game.i18n.format('DOCUMENT.New', {
          type: game.i18n.localize('DOCUMENT.ActiveEffect'),
        }),
        transfer: true,
      },
      { parent: this.item },
    );
    newEffect?.sheet?.render(true);
  }

  protected _toggleEffect(
    doc: SwadeActiveEffect,
    toggle: string,
  ): Record<string, unknown> {
    const oldVal = !!getProperty(doc, toggle);
    return { [toggle]: !oldVal };
  }

  private _rangeSuggestions() {
    return [
      '3/6/12',
      '4/8/16',
      '5/10/20',
      '10/20/40',
      '12/24/48',
      '15/30/60',
      '20/40/60',
      '20/40/80',
      '24/48/96',
      '25/50/100',
      '30/60/120',
      '50/100/200',
      '75/150/300',
      '300/600/1200',
    ];
  }

  private _equipStatusOptions(): Record<number, string> {
    let states: Record<number, string> = {
      [constants.EQUIP_STATE.STORED]: 'SWADE.ItemEquipStatus.Stored',
      [constants.EQUIP_STATE.CARRIED]: 'SWADE.ItemEquipStatus.Carried',
    };

    if (this.item.type === 'weapon') {
      if (this.item.system.isVehicular && this.actor?.type === 'vehicle') {
        states = {
          ...states,
          [constants.EQUIP_STATE.EQUIPPED]: 'SWADE.ItemEquipStatus.Installed',
        };
      } else {
        states = {
          ...states,
          [constants.EQUIP_STATE.MAIN_HAND]: 'SWADE.ItemEquipStatus.MainHand',
          [constants.EQUIP_STATE.OFF_HAND]: 'SWADE.ItemEquipStatus.OffHand',
          [constants.EQUIP_STATE.TWO_HANDS]: 'SWADE.ItemEquipStatus.TwoHands',
        };
      }
    } else if (this.item.type === 'armor' || this.item.type === 'shield') {
      states = {
        ...states,
        [constants.EQUIP_STATE.EQUIPPED]: 'SWADE.ItemEquipStatus.Equipped',
      };
    } else if (this.item.type === 'gear') {
      if (this.item.system.equippable) {
        states = {
          ...states,
          [constants.EQUIP_STATE.EQUIPPED]: 'SWADE.ItemEquipStatus.Equipped',
        };
      } else if (this.item.system.isVehicular) {
        states = {
          ...states,
          [constants.EQUIP_STATE.EQUIPPED]: 'SWADE.ItemEquipStatus.Installed',
        };
      }
    }
    return states;
  }

  private _trademarkWeaponOptions(): Record<number, string> {
    return {
      0: 'SWADE.TrademarkWeapon.None',
      1: 'SWADE.TrademarkWeapon.Regular',
      2: 'SWADE.TrademarkWeapon.Improved',
    };
  }

  private _reloadTypeOptions(): Record<string, string> {
    return {
      [constants.RELOAD_TYPE.NONE]: 'SWADE.ReloadType.None',
      [constants.RELOAD_TYPE.SELF]: 'SWADE.ReloadType.Self',
      [constants.RELOAD_TYPE.SINGLE]: 'SWADE.ReloadType.Single',
      [constants.RELOAD_TYPE.FULL]: 'SWADE.ReloadType.Full',
      [constants.RELOAD_TYPE.MAGAZINE]: 'SWADE.ReloadType.Magazine',
      [constants.RELOAD_TYPE.BATTERY]: 'SWADE.ReloadType.Battery',
      [constants.RELOAD_TYPE.PP]: 'SWADE.ReloadType.PP',
    };
  }
}

interface SwadeItemSheetData extends OptionsPartial {
  itemType: string;
  hasInlineDelete: boolean;
  isPhysicalItem: boolean;
  hasCategory: boolean;
  actionTypes: Record<string, string>;
  macroActorTypes: Record<string, string>;
  hasAdditionalStats: boolean;
  additionalStats: AdditionalStats;
  collapsibleStates: CollapsibleStates;
  isArcaneDevice: boolean;
  enrichedDescription: string;
  settingRules: {
    modSlots: boolean;
    noPowerPoints: boolean;
  };
  equipStatusOptions: Record<EquipState, string>;
  ranges: string[];
  trademarkWeaponOptions?: Record<number, string>;
  reloadTypeOptions?: Record<number, string>;
  embeddedPowers?: Map<string, ItemDataSource>;
  embeddedAbilities?: Array<Record<string, unknown>>;
  ammoList?: string[];
  ammoLoaded?: string;
  ppReload: boolean;
  abilityConfig?: {
    localization: typeof SWADE.abilitySheet;
    abilityHeader: string;
    isAncestryOrArchetype: boolean;
  };
  subtypes?: Record<string, string>;
  grantedItems?: ItemGrant[];
}

type OptionsPartial = Partial<ItemSheet.Data<DocumentSheetOptions>>;

interface CollapsibleStates {
  actions: Record<string, boolean>;
  powers: Record<string, boolean>;
  effects: Record<string, boolean>;
}
