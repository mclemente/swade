---
foundry:
  _key: '!journal.pages!8uC7RTgJOg8SW4cf.qKSga6mGzCPciZ8v'
  id: qKSga6mGzCPciZ8v
  name: NPC Sheet
  sort: 800000
---

NPC sheets are similar to the Character Sheet but much more compact and simplified. They are typically used for characters or creatures other than the player characters.

## Tweaks

NPC sheets have the same settings in Tweaks as character sheets plus one additional field.

**Wild Card:** Indicates whether or not the character or creature is a Wild Card. When checked, fields for tracking Bennies will be added to the sheet.

## Prototype Token

You can configure a default token directly from the sheet via the Prototype Token option in the character sheet’s title bar.

Details about Prototype Tokens can be found on the [Foundry VTT website](https://foundryvtt.com/article/tokens/).

## Items

All items on NPC sheets are displayed on the same page. You can add items to the NPC sheet by clicking on the “+” button in the respective section’s header.

If the NPC sheet includes an Edge that is marked as an Arcane Background, a Powers tab will be added to the sheet, and the main tab will be grouped under a Summary tab.

**Note:** The eye icon for each item behaves similarly to the chat icon on the character sheet. These icons will be redesigned in the near future to match the character sheet, including replacing the eye icon with the chat icon.
